
import validaciones
def agregar(agenda):    
    contacto = {}
    
    primerNombre = input("Ingrese el primer nombre: ")
    apellidoPaterno = input("Ingrese el apellido parterno: ")
    telefono = input("Ingrese el número de teléfono: ")
    email = input("Ingrese el email: ")
    dni = input("Ingrese el DNI: ")

    contacto = {"primerNombre":primerNombre,
                "apellidoPaterno":apellidoPaterno,
                "telefono":telefono,
                "email":email,
                "dni":dni}
        
    return agenda.append(contacto)

def listar(agenda):
    print ("== Listado de agenda == ")
    for x in agenda: 
        print(x)

def buscar(agenda):
        pos=-1
        while True:
            print("Buscar por:")
            print("1 : Nombre")
            print("2 : DNI")
            opc=0
            try:
                opc=int(input("Seleccionar: "))
                
            except ValueError:
                print("Opcion no valida")
                break
            if opc==1:
                nombre=input("Ingrese Nombre:")
                p=0
                for i in agenda:
                    if i["primerNombre"].find(nombre,0,len(nombre))>=0:
                        #print("Encontrado pos: "+str(p))
                        print(i)
                        pos=p
                        #break
                    p=p+1
                break
            elif opc==2:
                    try:
                        dni=input("Ingrese DNI:")
                        p=0
                        for i in agenda:
                            #validar dni
                            r=validaciones.valida_DNI(dni)
                            if r==1:
                                if i["dni"].find(str(dni))>=0:
                                        #print("DNI encontrado pos: "+str(p))
                                        print(i)
                                        pos=p
                                        break
                            else:
                                break
                            p=p+1
                        break
                    except ValueError:
                        print("Valor no valido")
        return pos

def eliminar(agenda):
        dni=input("Ingrese DNI del contacto a eliminar:")
        p=0
        for i in agenda:
            #validar dni
            r=validaciones.valida_DNI(dni)
            if r==1:
                if i["dni"].find(str(dni))>=0:
                    print("DNI encontrado pos: "+str(p))
                    
                    print("Contacto eliminado: "+str(i["primerNombre"]))
                    agenda.pop(p)
                    break
            else:
                break
            p=p+1